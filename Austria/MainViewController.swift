//
//  ViewController.swift
//  Austria
//
//  Created by Frederick Lee on 10/22/21.
//

import AustriaModel
import UIKit

class MainViewController: UIViewController {
    @IBOutlet var tableView: UITableView!

    var provinces: [RevisedAustriaProvince]?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }

    @IBAction func asyncAction(_ sender: UIBarButtonItem) {
        Task {
            let austriaModel = AustriaModel.shared
            let result = await austriaModel.getAsyncData()
            self.provinces = result!.provinces

            self.tableView.reloadData()
        }
    }

    @IBAction func clearAction(_ sender: UIBarButtonItem) {
        provinces?.removeAll()
        tableView.reloadData()
    }

    @IBAction func exitAction(_ sender: UIBarButtonItem) {
        exit(0)
    }
}
